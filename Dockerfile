# use https://gitlab.com/antora/docker-antora/
#
FROM antora/antora:3.1.4

# add Antora extensions
#
RUN yarn global add --ignore-optional --silent @antora/lunr-extension

# add Bash, Git and Ruby
#
RUN apk add --no-cache bash>4.0.0 git ruby>3.1.0

# prefered diagram processors supported by asciidoctor-diagram for later use
#
RUN apk add --no-cache gnuplot graphviz

# install (Ruby versons) Asciidoctor and any extensions
#
RUN gem install asciidoctor \
    && gem install asciidoctor-diagram \
    && gem install asciidoctor-pdf

# Asciidoctor reveal.js
# https://docs.asciidoctor.org/reveal.js-converter/latest/setup/ruby-setup/
#
RUN gem install asciidoctor-revealjs \
    && gem install bundler \
    && git clone -b 4.3.1 --depth 1 https://github.com/hakimel/reveal.js.git

# add Python for later use
#
RUN apk add --update --no-cache python3=~3.11 py3-pip
